This project uses [webpack-starter-basic](https://github.com/lifenautjoe/webpack-starter-basic) as a build system, which includes the following features:

* Separated development and production webpack settings
* Sass
* ES6
* Asset loading
* CSS Vendor prefixing
* Development server
* Sourcemaps
* Favicons generation
* Production optimizations
* Mobile browser header color

### Usage steps:

#### Installation
```sh
git clone git@bitbucket.org:humbaba/gin.git Gin
cd Gin
npm install
```

#### Running in development
(serves compiled sources from memory at **localhost:8080**)
```sh
npm start
```

#### Running in development
(builds the project in the `dist` folder and serves it at **localhost:8080**)
```sh
npm run preview
```

#### Building the bundles for production
(builds the project in the `dist` folder)
```sh
npm run build
```
