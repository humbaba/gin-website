require('./styles/index.scss');
window.$ = require('jquery');

((document, window) => {
    //generic throttle helper function for computationally intensive DOM events
    const throttle = (func, wait, options) => {
        let context, args, result;
        let timeout = null;
        let previous = 0;
        if (!options) options = {};
        let later = () => {
            previous = options.leading === false ? 0 : Date.now();
            timeout = null;
            result = func.apply(context, args);
            if (!timeout) context = args = null;
        };
        return () => {
            let now = Date.now();
            if (!previous && options.leading === false) previous = now;
            let remaining = wait - (now - previous);
            context = this;
            args = arguments;
            if (remaining <= 0 || remaining > wait) {
                if (timeout) {
                    clearTimeout(timeout);
                    timeout = null;
                }
                previous = now;
                result = func.apply(context, args);
                if (!timeout) context = args = null;
            } else if (!timeout && options.trailing !== false) {
                timeout = setTimeout(later, remaining);
            }
            return result;
        };
    };

    let headerRef;
    let backdropRef;
    let mobileBtnRef;
    let navMenuRef;

    let prevScrollTop = window.scrollY;

    const grabDOMRefs = () => {
        headerRef = document.querySelector('.header');
        backdropRef = document.querySelector('.page-backdrop');
        mobileBtnRef = document.querySelector('.mobile-menu');
        navMenuRef = document.querySelector('.main-nav');
    };

    const attachListeners = () => {
        $(window).scroll(throttle(handleScroll, 50));
        $(mobileBtnRef).click(() => {
            $(headerRef).toggleClass('opaque');
            $(backdropRef).fadeToggle(300);
            $(navMenuRef).toggleClass('unconcealed');
            $('html').toggleClass('no-scroll');
        });
        $(backdropRef).click((ev) => {
            $(headerRef).removeClass('opaque');
            $(ev.target).fadeOut(300);
            $(navMenuRef).removeClass('unconcealed');
            $('html').removeClass('no-scroll');
        });
    };

    const handleSplashAnimation = () => {
        if (window.scrollY === 0) {
            document.querySelector('.header-logo').classList.add('invisible');
            document.querySelector('.splash').classList.add('animated');
        }
        document.querySelector('body').classList.remove('no-scroll');
        document.querySelector('.splash').classList.add('finished');
        window.setTimeout(() => {
            document.querySelector('.header-logo').classList.remove('invisible');
            document.querySelector('.splash').classList.add('hidden');
        }, 300);
    };

    const handleScroll = () => {
        if (window.scrollY >= (1.5 * headerRef.offsetHeight)) {
            headerRef.classList.add('sticky');
            if (window.scrollY >= prevScrollTop) {
                headerRef.classList.add('concealed');
            } else {
                headerRef.classList.remove('concealed');
            }
        } else {
            headerRef.classList.remove('sticky');
        }
        prevScrollTop = window.scrollY;
    };

    document.addEventListener('DOMContentLoaded', () => {
        grabDOMRefs();
    });

    window.addEventListener('load', () => {
        handleSplashAnimation();
        attachListeners();
    });

})(document, window);
