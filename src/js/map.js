(() => {
    const mapWrapperRef = document.querySelector('.hero-map');
    const movingTooltipRef = document.querySelector('.moving-tooltip');
    let mapPingDotRefs = [];
    let mapSvgRef;
    let mapServerLocationData = [];

    let servers;
    let coins;
    let summary;

    const startMapIntervalPings = () => {
        let lastRandomDotIndex;
        window.setInterval(() => {
            let currentRandomDotIndex;

            do {
                currentRandomDotIndex = Math.floor(Math.random()*(mapPingDotRefs.length));
            } while (currentRandomDotIndex === lastRandomDotIndex)

            movingTooltipRef.style.top = mapPingDotRefs[currentRandomDotIndex].style.top;
            movingTooltipRef.style.left = mapPingDotRefs[currentRandomDotIndex].style.left;
            movingTooltipRef.innerText =
                `${coins[Math.floor(Math.random()*coins.length)].symbol}\nNew node in ${mapPingDotRefs[currentRandomDotIndex].getAttribute('data-name')}`;
            movingTooltipRef.classList.add('visible');
            mapPingDotRefs[currentRandomDotIndex].classList.add('ripple');

            window.setTimeout(() => {
                movingTooltipRef.classList.remove('visible');
                mapPingDotRefs[currentRandomDotIndex].classList.remove('ripple');
            }, 1500);

            lastRandomDotIndex = currentRandomDotIndex;
        }, 3000)
    };

    const processNodes = () => {
        mapSvgRef = document.querySelector('.hero-map object').contentDocument.querySelector('svg');
        const svgWidth = mapSvgRef.getBBox().width;
        const svgHeight = mapSvgRef.getBBox().height;

        servers.forEach(location => {
            const currServerDot = mapSvgRef.querySelector(`#path${location.mapNodeNr}`);
            const currXPos = (currServerDot.getBBox().x / svgWidth);
            const currYPos = (currServerDot.getBBox().y / svgHeight);
            mapServerLocationData = mapServerLocationData.concat(
                {
                    id: location.id,
                    xPos: currXPos,
                    yPos: currYPos,
                    name: location.name
                }
            );
        });

        if (window.innerWidth > 980) {
            mapServerLocationData.forEach(location => {
                const pingDot = document.createElement('div');
                pingDot.classList.add('hero-map-dot');
                pingDot.style.top = (location.yPos * 100) + '%';
                pingDot.style.left = (location.xPos * 100) + '%';
                pingDot.setAttribute('data-id', location.id);
                pingDot.setAttribute('data-name', location.name);
                mapWrapperRef.appendChild(pingDot);
                mapPingDotRefs = mapPingDotRefs.concat(pingDot);
            });
            startMapIntervalPings();
        }
    };

    const init = () => {
        $.ajax({
            method: 'get',
            url: 'http://localhost:3000/data',
            dataType: 'json'
        })
        .done(response => {
            summary = response.summary;
            servers = response.locations;
            coins = response.coins;
        })
        .fail(error => {
            console.error(error.statusText, error.statusMessage);
            servers = [
                {id: 1, name: 'Berlin', mapNodeNr: '30'},
                {id: 2, name: 'Washington', mapNodeNr: '315'},
                {id: 3, name: 'Cairo', mapNodeNr: '900'},
                {id: 4, name: 'Tel Aviv', mapNodeNr: '879'},
                {id: 5, name: 'San Francisco', mapNodeNr: '284'},
                {id: 6, name: 'Seattle', mapNodeNr: '339'},
                {id: 7, name: 'Shanghai', mapNodeNr: '891'},
                {id: 8, name: 'London', mapNodeNr: '772'},
                {id: 9, name: 'Stockholm', mapNodeNr: '467'},
                {id: 10, name: 'Sidney', mapNodeNr: '85'},
                {id: 11, name: 'Singapore', mapNodeNr: '941'},
                {id: 12, name: 'Mumbai', mapNodeNr: '190'},
                {id: 13, name: 'Rio de Janeiro', mapNodeNr: '2'}
            ];
            coins = [
                {id: 1, symbol: 'GIN'},
                {id: 2, symbol: 'XLQ'},
                {id: 3, symbol: 'SYS'},
                {id: 4, symbol: 'BWK'},
                {id: 5, symbol: 'ZEN'},
                {id: 6, symbol: 'MCT'},
                {id: 7, symbol: 'IFX'},
                {id: 8, symbol: 'XZC'},
                {id: 9, symbol: 'DASH'}
            ]
        })
        .always(() => {
            processNodes();
        })
    };

    window.addEventListener('load', () => {
        init();
    });
})();
