(() => {
    const homeVideoUrl = 'suY0EZaPgHE';
    const scriptTag = document.createElement('script');
    scriptTag.src = 'http://www.youtube.com/iframe_api';
    scriptTag.type = 'application/javascript';
    const firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(scriptTag, firstScriptTag);

    let videoPlayer;

    window.onYouTubeIframeAPIReady = () => {
        videoPlayer = new YT.Player('player', {
            videoId: homeVideoUrl,
            playerVars: {
                'autoplay': 0,
                'rel': 0,
                'showinfo': 0
            },
            events: {
                'onStateChange': onPlayerStateChange
            }
        });
    };

    const onPlayerStateChange = (ev) => {
        window.setTimeout(() => {
            if (ev.target.getPlayerState() === 2) {
                if (window.innerWidth > 980) {
                    $('#video-textbox').show();
                }
                $('#video-background').show();
                $('#player').hide();
            }
        }, 500);
    };

    $('#player').hide();
    document.querySelector('#video-thumbnail').src = `http://img.youtube.com/vi/${homeVideoUrl}/hqdefault.jpg`;
    $('#video-play-btn').click(() => {
        if (window.innerWidth > 980) {
            $('#video-textbox').hide();
        }
        $('#video-background').hide();
        $('#player').show();
        videoPlayer.playVideo();
    });
})();
