(() => {
    const onboardingHolderRef = document.querySelector('.onboarding-holder');
    const onboardingTooltipRef = document.querySelector('.onboarding-tooltip');
    let onboardingPulseRefs = [];
    let indications;

    const processIndications = () => {
        if (window.innerWidth > 980) {
            indications.forEach(ind => {
                const pulse = document.createElement('div');
                pulse.classList.add('onboarding-pulse');
                pulse.style.top = ind.yPos + '%';
                pulse.style.left = ind.xPos + '%';
                pulse.setAttribute('data-id', ind.id);
                onboardingHolderRef.appendChild(pulse);
                onboardingPulseRefs = onboardingPulseRefs.concat(pulse);
                pulse.addEventListener('click', toggleTooltip);
            });
        }
    };

    const toggleTooltip = (event) => {
        onboardingPulseRefs.forEach(elem => {elem.classList.remove('active')});
        event.target.classList.add('active');
        const currId = event.target.getAttribute('data-id');
        const currIndication = indications.find(elem => elem.id.toString() === currId);
        $(onboardingTooltipRef).fadeOut(300);

        if (onboardingTooltipRef.style.display === 'none') {
            displayTooltip(onboardingTooltipRef, currIndication);
        } else {
            window.setTimeout(() => {
                displayTooltip(onboardingTooltipRef, currIndication);
            }, 300)
        }
    };

    const displayTooltip = (domRef, data) => {
        domRef.style.top = data.yPos + '%';
        domRef.style.left = data.xPos + '%';
        domRef.querySelector('h4').innerText = data.title;
        domRef.querySelector('p').innerText = data.text;
        $(domRef).fadeIn(300);
    };

    const init = () => {
        $.ajax({
            method: 'get',
            url: 'http://localhost:3000/data',
            dataType: 'json'
        })
            .done(response => {
                indications = response.onboarding;
            })
            .fail(error => {
                console.error(error.statusText, error.statusMessage);
                indications = [
                    {id: 1, yPos: 20.469, xPos: 26.2626, title: 'Goddamn cool onboarding title', text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus!' },
                    {id: 2, yPos: 30.447, xPos: 7.839, title: 'Cool onboarding title', text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus!' },
                    {id: 3, yPos: 25.055, xPos: 76.068, title: 'Nice onboarding title', text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus!' },
                    {id: 4, yPos: 61.521, xPos: 40.17, title: 'Goddamn cool onboarding title', text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus!' },
                    {id: 5, yPos: 78.076, xPos: 23.465, title: 'Goddamn awesome title', text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus!' }
                ]
            })
            .always(() => {
                processIndications();
                $(onboardingTooltipRef).on('click', '.btn-close', (function() {
                    $(this).parent().fadeOut(300);
                }));
            })
    };

    window.addEventListener('load', () => {
        init();
    });
})();
