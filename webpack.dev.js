const path = require('path');
const buildPath = path.resolve(__dirname, 'dist');

const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    devtool: 'eval-cheap-module-source-map',
    entry: {
        app: './src/index.js',
        map: './src/js/map.js',
        player: './src/js/player.js',
        tooltips: './src/js/onboarding-tooltip.js'
    },
    devServer: {
        port: 8080,
        publicPath : '/',
        historyApiFallback: {
            rewrites: [
                { from: /^\/$/, to: '/index.html' },
                { from: /^\/team/, to: '/team.html' },
                { from: /^\/roadmap/, to: '/roadmap.html' },
                { from: /^\/investor/, to: '/investor.html' },
                { from: /^\/coin/, to: '/coin.html' },
                { from: /^\/press/, to: '/press.html' },
                { from: /^\/blog/, to: '/article.html' },
                // { from: /(.*)/, to: '/not-found.html' }
            ],
        },
        contentBase: buildPath,
        watchContentBase: true
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
                options: {
                    presets: ['env']
                }
            },
            {
                test: /\.(scss|css)$/,
                use: [
                    {
                        // creates style nodes from JS strings
                        loader: 'style-loader',
                        options: {
                            sourceMap: true
                        }
                    },
                    {
                        // translates CSS into CommonJS
                        loader: 'css-loader',
                        options: {
                            sourceMap: true
                        }
                    },
                    {
                        // compiles Sass to CSS
                        loader: 'sass-loader',
                        options: {
                            outputStyle: 'expanded',
                            sourceMap: true,
                            sourceMapContents: true
                        }
                    }
                    // Please note we are not running postcss here
                ]
            },
            {
                // Load all images as base64 encoding if they are smaller than 8192 bytes
                test: /\.(png|jpg|gif|svg)$/,
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            // On development we want to see where the file is coming from, hence we preserve the [path]
                            name: '[path][name].[ext]?hash=[hash:20]',
                            limit: 8192
                        }
                    }
                ]
            },
            {
                test: /\.(ttf|eot|woff|woff2)$/,
                loader: 'file-loader',
                options: {
                    name: 'fonts/[name].[ext]',
                },
            },
            {
                test: /\.(html)$/,
                use: {
                    loader: 'html-loader',
                    options: {
                        interpolate: true
                    }
                }
            }
        ],
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: './src/index.ejs',
            filename: 'index.html',
            chunks: ['app', 'map', 'player'],
            inject: true
        }),
        new HtmlWebpackPlugin({
            template: './src/team.ejs',
            filename: 'team.html',
            chunks: ['app'],
            inject: true
        }),
        new HtmlWebpackPlugin({
            template: './src/roadmap.ejs',
            filename: 'roadmap.html',
            chunks: ['app'],
            inject: true
        }),
        new HtmlWebpackPlugin({
            template: './src/investor.ejs',
            filename: 'investor.html',
            chunks: ['app', 'tooltips'],
            inject: true
        }),
        new HtmlWebpackPlugin({
            template: './src/coin.ejs',
            filename: 'coin.html',
            chunks: ['app', 'player'],
            inject: true
        }),
        new HtmlWebpackPlugin({
            template: './src/press.ejs',
            filename: 'press.html',
            chunks: ['app'],
            inject: true
        }),
        new HtmlWebpackPlugin({
            template: './src/article.ejs',
            filename: 'article.html',
            chunks: ['app'],
            inject: true
        })
    ]
};
